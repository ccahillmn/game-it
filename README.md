Copyright 2015 Caitlin Cahill

Raw code available in App folder. The custom polymer elements I created are in the Elements folder. Libraries and plugins developed by others have a license and/or copyright at the top of the document, and are mostly located in the Plugins and Bower Components folders.