var cy;
var nodeId;
var items;
var paths;
var animation = true;

//Initialize map
function mapQuest(items,paths){

        cytoscape({
          container: document.getElementById('map'),
          layout: {
            name: 'breadthfirst',
            padding: 0,
            avoidOverlap:true,
            fit: true,
            directed: true,
            animate:animation
          },
          
          style: cytoscape.stylesheet()
            .selector('node')
              .css({
                'shape': 'circle',
                'width': '70',
                'height': '70',
                'background-image': 'data(pic)',
                'background-repeat':'none',
                'background-width':'100%',
                'background-height':'100%',
                'text-valign': 'center',
                'text-outline-width': 2,
                'background-color': 'white',
                'border-width': '3px' ,
                'border-color':'white',
                'color': 'white',
                'shadow-color':'black',
                'shadow-offset-x':4,
                'shadow-offset-y':4,
                'shadow-blur':4,
                'shadow-opacity':.9
              })
            .selector('edge')
              .css({
                'curve-style':'unbundled-bezier',
                'control-point-distance':'data(curve)',
                'line-style':'dashed',
                'width':'2',
                'shadow-color':'black',
                'shadow-offset-x':2,
                'shadow-offset-y':2,
                'shadow-blur':3,
                'shadow-opacity':.8
              })
            .selector('node.complete')
              .css({
                'width': '50',
                'height': '50',
                'background-color':'yellow',
                'border-color': 'GoldenRod',
                'color':'GoldenRod',
                'shadow-color':'none',
                'shadow-offset-x':0,
                'shadow-offset-y':0,
                'shadow-opacity':0
              })
            .selector('edge.complete')
              .css({
                'line-color':'GoldenRod',
                'width': '5',
                'line-style':'solid',
                'shadow-color':'none',
                'shadow-offset-x':0,
                'shadow-offset-y':0,
                'shadow-opacity':0
              })
            .selector('node:selected')
              .css({
                'border-width': 3,
                'border-color': '#333',
                'width': '70',
                'height': '70',
              })
            .selector('#badge')
              .css({
                'shape':'star',
                'width':'75',
                'height':'70',
                'border-color':'#fbc02d',
                'border-width': '4px' ,
                'text-outline-width': 1,
              }),
            
          
          elements: {
            nodes: items,
            edges: paths
          },
          
          ready: function(){

            cy = this;
            
            cy.on('layoutstop',function(){
              animation = false;
              badge();
            });

            cy.on('select','node', function(e){
              var node = e.cyTarget;
              var nodeId = node.data('id');
              var isFinal = node.hasClass('final');

              // Show activity details on click
              if(nodeId != 'badge'){
                app.activity = nodeId;
                app.isFinal = isFinal;
                document.getElementById('activity-details').open();
              }
            });

            // Relayout map when nodes changed
            cy.on('add','node',function(e){
              animation = false;
              cy.layout();
            })

            cy.on('remove', 'node',function(e){
              animation = false;
              cy.layout();
            })

          },       

        });

        }

function unselect(){
  cy.$().unselect();
}

function delNode(node){
  cy.$(node).remove();
}

// Add node when new activity created
function addNode(node){
  var data = [];
  var newNode;
  activities.child(node).once('value',function(s){
    newNode = s.val();
    var key = s.key();
    var id = {'id':key};
    $.extend(newNode,id);
    data.push({group:'nodes',data:newNode});

    activities.child(key).child('parents').once('value',function(snap){
      snap.forEach(function(p){
        var parent = p.key();
        var curve = Math.floor(Math.random() * 101) - 50;
        data.push({group:'edges','data':{'source':parent,'target':key,'curve':curve}});
      });
    });
  })
  cy.add(data);
}

function unmap(){
    cy.nodes().remove();
    cy.edges().remove();
    animation = true;
}

// Add badge image at end of map
function badge(){
  var data = [];
  var newNode;
  
  // get quest badge
    badges.child(app.questId).once("value",function(s){
      newNode = s.val();
      var id = {'id':'badge'};
      $.extend(newNode,id);
      data.push({group:'nodes',data:newNode});

      // get leaves and add path to badge
      var nodes = cy.nodes().not('#badge'); 
      var leaves = nodes.leaves();
      leaves.forEach(function(ele){
        ele.addClass('final');
        var curve = Math.floor(Math.random() * 101) - 50;
        var parent = ele.id();
        var classes;
        if(ele.hasClass('complete')){
          classes = 'complete'
        }
        data.push({group:'edges','data':{'source':parent,'target':'badge','curve':curve,},'classes':classes});
      });

    cy.add(data);
  });

}