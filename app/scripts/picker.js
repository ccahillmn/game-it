// The Browser API key obtained from the Google Developers Console.
var developerKey = 'AIzaSyByMSamVCqVaV7-gx7G0oSDr68dvZ0-Myc';

// The Client ID obtained from the Google Developers Console. Replace with your own Client ID.
var clientId = '516602267802-0aa2k0l5e32o39tc0v2ekskocuijrce4.apps.googleusercontent.com';

// Scope to use to access user's photos.
var scope = ['https://www.googleapis.com/auth/photos.upload'];

var pickerApiLoaded = false;
var oauthToken;
var myImg;
var picker;

function loadPicker(element){
  myImg = element;
  gapi.load('auth', {'callback': onAuthApiLoad});
  gapi.load('picker', {'callback': onPickerApiLoad});
}

function onAuthApiLoad() {
  window.gapi.auth.authorize(
      {
        'client_id': clientId,
        'scope': scope,
        'immediate': false
      },
      handleAuthResult);
}

function onPickerApiLoad() {
  pickerApiLoaded = true;
}

function handleAuthResult(authResult) {
  if (authResult && !authResult.error) {
    oauthToken = authResult.access_token;
    createPicker();
  }
}

// Create and render a Picker object for picking user photos
function createPicker() {
  if (pickerApiLoaded && oauthToken) {
    picker = new google.picker.PickerBuilder().
        addView(google.picker.ViewId.PHOTO_UPLOAD).
        addView(google.picker.ViewId.PHOTOS).
        setOAuthToken(oauthToken).
        setDeveloperKey(developerKey).
        setCallback(pickerCallback).
        build();
    
    picker.setVisible(true);
  }
}

// Callback action when picker closed
function pickerCallback(data) {
  var url;
  if (data[google.picker.Response.ACTION] == google.picker.Action.PICKED) {
    var doc = data[google.picker.Response.DOCUMENTS][0];
    var thumbs = data.docs[0].thumbnails;
    url = thumbs[thumbs.length - 1].url;
    $(myImg).attr('src',url);
    myImg = null;
    picker.dispose();
  }
  else if(data[google.picker.Response.ACTION] == google.picker.Action.CANCEL){
    picker.dispose();
  }
}




