var app = document.querySelector('#app');

// Firebase References
var ref = new Firebase('radiant-torch-7856.firebaseIO.com/');
var users = new Firebase('radiant-torch-7856.firebaseIO.com/users');
var games = new Firebase('radiant-torch-7856.firebaseIO.com/games');
var invitees = new Firebase('radiant-torch-7856.firebaseIO.com/invitees');
var activities = new Firebase('radiant-torch-7856.firebaseIO.com/activities');
var quests = new Firebase('radiant-torch-7856.firebaseIO.com/quests');
var badges = new Firebase('radiant-torch-7856.firebaseIO.com/badges');
var behaviors = new Firebase('radiant-torch-7856.firebaseIO.com/behaviors');
var logs = new Firebase('radiant-torch-7856.firebaseIO.com/logs');
var points = new Firebase('radiant-torch-7856.firebaseIO.com/points');
var rankings = new Firebase('radiant-torch-7856.firebaseIO.com/rankings');

// User Data
var authData;
var myEmail;
var myUsername;
var me;
var gameID;

(function(document) {
  'use strict';

  // Grab a reference to our auto-binding template
  // and give it some initial binding values
  // Learn more about auto-binding templates at http://goo.gl/Dx1u2g
  var app = document.querySelector('#app');

  app.displayInstalledToast = function() {
    document.querySelector('#caching-complete').show();
  };

  // Listen for template bound event to know when bindings
  // have resolved and content has been stamped to the page
  app.addEventListener('dom-change', function() {
    console.log('Our app is ready to rock!');
  });

  // See https://github.com/Polymer/polymer/issues/1381
  window.addEventListener('WebComponentsReady', function() {
    // imports are loaded and elements have been registered
  });

  // Main area's paper-scroll-header-panel custom condensing transformation of
  // the appName in the middle-container and the bottom title in the bottom-container.
  // The appName is moved to top and shrunk on condensing. The bottom sub title
  // is shrunk to nothing on condensing.
  addEventListener('paper-header-transform', function(e) {
    var appName = document.querySelector('.app-name');
    var middleContainer = document.querySelector('.middle-container');
    var bottomContainer = document.querySelector('.bottom-container');
    var detail = e.detail;
    var heightDiff = detail.height - detail.condensedHeight;
    var yRatio = Math.min(1, detail.y / heightDiff);
    var maxMiddleScale = 0.50;  // appName max size when condensed. The smaller the number the smaller the condensed size.
    var scaleMiddle = Math.max(maxMiddleScale, (heightDiff - detail.y) / (heightDiff / (1-maxMiddleScale))  + maxMiddleScale);
    var scaleBottom = 1 - yRatio;

    // Move/translate middleContainer
    Polymer.Base.transform('translate3d(0,' + yRatio * 100 + '%,0)', middleContainer);

    // Scale bottomContainer and bottom sub title to nothing and back
    Polymer.Base.transform('scale(' + scaleBottom + ') translateZ(0)', bottomContainer);

    // Scale middleContainer appName
    Polymer.Base.transform('scale(' + scaleMiddle + ') translateZ(0)', appName);
  });

  // Close drawer after menu item is selected if drawerPanel is narrow
  app.onMenuSelect = function() {
    var drawerPanel = document.querySelector('#paperDrawerPanel');
    if (drawerPanel.narrow) {
      drawerPanel.closeDrawer();
    }
  };


//////////// My Scripts //////////

  // Update global variables on login
  if(ref.getAuth()){
    authData = ref.getAuth();
    myEmail = authData.google.email;
    myUsername = myEmail.replace(/[^\w\s]/gi, '');
    me = authData.uid;

    countGames(me,myEmail,myUsername);

    app.questId = null;
    app.badgeId = null;
  }

  // Prevent page refresh when game name updated
  $(document.body).on('keydown', '[contenteditable]' ,function(e){
   if(e.which === 13) {
      $(this).blur();
      e.preventDefault();
    }
  });

  // Edit Game Name
  app.updateName = function(){
    var newName = $(event.target).text();
    console.log(newName);
    $.trim(newName);

    if(newName){
      var URL = newName.replace(/\s+/g, '').toLowerCase();
      games.child(gameID).update({name:newName,URL:URL});

    }
    else{
      newName = app.thisGame.name;
    }
  };

  ////////// Manage Games //////////

  // Leave Game
  app.leaveGame = function(){
    if(confirm('Are you sure you want to leave this game?')){
      unenroll(authData.uid);
      location.pathname = '/';
    }
  };

  ////////// Animate pages on tab change //////////
  app.tab = 0;
  var prevTab = 0;

  app.tabChange = function(){
    var newTab = app.tab;
    var content = document.getElementById('mainContainer')
    content.scrollTop = 0;

    if(newTab > prevTab){
      app.entryAnimation = 'slide-from-right-animation';
      app.exitAnimation = 'slide-left-animation';
    }
    else{
      app.entryAnimation = 'slide-from-left-animation';
      app.exitAnimation = 'slide-right-animation';
    }
    app.page = newTab;
    prevTab = newTab;
  }

  ////////// Helper Functions //////////

  // Unenroll from Game
  function unenroll(player){
    games.child(gameID+'/players/'+player).remove();
  };

  function openDialog(id){
    var dialog = document.getElementById(id);
    if (dialog) {
      dialog.show();
    }
  }


})(document);

// Determine whether user has games
function countGames(me,myEmail,myUsername){
  users.child(me).child('master').on('value',function(s){
    app.master = s.val();
  });

  games.orderByChild('admins/'+me).equalTo(myEmail).on('value', function(s){
    app.facilitating = s.exists();
  });

  games.orderByChild('players/'+me).equalTo(myEmail).on('value', function(s){
    app.playing = s.exists();
    if(app.playing === true){
      app.hasGames = true;
    }
  });

  invitees.orderByChild(myUsername).equalTo(myEmail).on('value', function(s){
    app.pending = s.exists();
    if(app.playing === true){
      app.hasGames = true;
    }
  });
}

// Update interface and global variables when user navigates to new game
function setGame(authData){
  $('iron-collapse').hide();
  $('.box .toggle iron-icon').addClass('rotate');

  games.child(gameID).child('players').on('value',function(p){
    app.players = 0;
    if(p.exists()){
      p.forEach(function(){
        app.players++;
      });
    }
  });

  invitees.child(gameID).on('value',function(i){
    app.invitees = 0;
    if(i.exists()){
      i.forEach(function(){
        app.invitees++;
      });
    }
  });

  games.child(gameID).child('admins').on('value',function(s){
    app.admin = s.hasChild(authData.uid);
  });

};

// Add a new game
function addGame(name){
  var uid = app.user.uid;
  var email = app.user.google.email;
  var name = $.trim(name);
  var input = $('[name="new-game-name"]');
  var date = new Date().getTime();

  if(name){

    URL = name.replace(/\s+/g, '').toLowerCase();

    var newPostRef = games.push({
      name : name,
      URL : URL,
      reset: date
    });

    newPostRef.child('admins/'+uid).set(email);

    input.value = '';
  }

  event.preventDefault();
  return false;
      
}

// Reset game leaderboard
function resetLB(game){
  var date = new Date().getTime();
  games.child(game +'/reset').set(date);
  points.child(game).once("value",function(u){
    u.forEach(function(p){
      var user = p.key();
      points.child(game+'/'+user+'/current').set(0);
    });
  });
  toast("Leaderboard has been reset.");
}

// Show temporary alert
function toast(text){
  app.toastText = text;
  document.querySelector('#toast').show();
}


